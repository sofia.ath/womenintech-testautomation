*** Settings ***
Documentation     Simple example using SeleniumLibrary.
Library           SeleniumLibrary
Suite Setup     Open Browser    ${URL}   ${BROWSER}
Suite Teardown  Close All Browsers

*** Variables ***
${URL}              http://www.google.com
${BROWSER}          Chrome
${search_form}      css=form[name=f]
${search_query}     css=input[name=q]
${search_value}     Woman In Tech 2020
${WiT_query}        css=div[class=yuRUbf]

*** Test Cases ***
Google Search
    Open Browser To Google Page
    Fill search with   ${search_value}
    Submit Form

*** Keywords ***
Open Browser To Google Page
    Wait Until Element Is Visible  ${search_form}
    Wait Until Element Is Visible  ${search_query}
    Title Should Be    Google

Fill search with
    [Arguments]    ${search_term}
    Input Text      ${search_query}   ${EMPTY}
    Input Text      ${search_query}   ${search_term}
*** Keywords ***
Open Browser To Survey Page
    Title Should Be    Survey App

Fill 1st question
    [Arguments]    ${data}
    Input Text    question1    ${data}

Fill 2nd question
    [Arguments]    ${data}
    Input Text    question2    ${data}

Fill 3rd question
    [Arguments]    ${data}
    Input Text    question3    ${data}

Fill 4th question
    [Arguments]    ${data}
    Select From List By Index    question4    1

Fill 5th question
    [Arguments]    ${data}
    Input Text    question5    ${data}

Submit the Survey
    Click Button    //*[@id="page-content-wrapper"]/div/div/div/form/button

No error should be returned
    Title Should Be    Survey App

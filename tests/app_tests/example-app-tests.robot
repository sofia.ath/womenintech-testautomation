*** Settings ***
Documentation     Multiple Tests example using SeleniumLibrary.
Resource          example-app-common.robot
Library           SeleniumLibrary
Variables         example-app-variables.yaml
Suite Teardown    Close All Browsers

*** Test Cases ***
TC01 - Test of example app - inital data
    [Documentation]    With this test we want to load the first_scenario data
    Populate the page with the first_scenario data

TC02 - Test of example app - second data
    [Documentation]    With this test we want to load the second_scenario data
    Populate the page with the second_scenario data

#TC03 - Test of example app - incorrect data
#    [Documentation]    With this test we want to load the negative_scenario_1 data
#    Populate the page with the negative_scenario_1 data


*** Keywords ***
Populate the page with the ${type} data
    Open Browser    ${URL}   ${BROWSER}
    Open Browser To Survey Page
    Fill 1st question    ${ ${type}.Q1 }
    Fill 2nd question    ${ ${type}.Q2 }
    Fill 3rd question    ${ ${type}.Q3 }
    Fill 4th question    ${ ${type}.Q4 }
    Fill 5th question    ${ ${type}.Q5 }
    Submit the Survey
    No error should be returned
    Close All Browsers